from webservices.serializers import *
from django.http import JsonResponse
from rest_framework.response import Response
from rest_framework.decorators import api_view, permission_classes
from rest_framework import permissions
from rest_framework import status
from rest_framework import generics
try:
    import json
except:
    import django.utils.simplejson as json


@api_view(['GET'])
@permission_classes((permissions.AllowAny,))
def naatofartistgeturl(request, artistid):
    try:
        instance = Naat.objects.filter(id=artistid)
    except Naat.DoesNotExist:
        instance = None
    if instance != None:
        serializer = NaatSerializer(instance, many=True)
        return JsonResponse(serializer.data, status=status.HTTP_200_OK,safe=False)
    else:
        return JsonResponse("sorrry id not exist",status=status.HTTP_400_BAD_REQUEST,safe=False)

@api_view(['GET'])
@permission_classes((permissions.AllowAny,))
def singleartistget(request, artistid):
    try:
        instance = Artist.objects.get(id=artistid) 
    except Artist.DoesNotExist:
        instance = None
    if instance != None:
        serializer = ArtistSerializer(instance)
        return JsonResponse(serializer.data, status=status.HTTP_200_OK,safe=False)
    else:
        return JsonResponse("sorrry id not exist",status=status.HTTP_400_BAD_REQUEST,safe=False)



from django.http import HttpResponse
@api_view(['GET'])
@permission_classes((permissions.AllowAny,))
def naatlist(request):
    dataaa = Naat.objects.all()
    serializer = NaatSerializer(dataaa, many=True)
    message = "Yaaahooo!"
    statuscode = 200
    statuslist = []
    statuslist.append(statuscode)
    statuslist.append(message)
    newdict = {'statuscode': 200, 'message': "Naat List"}
    # dump = json.dumps(serializer.data)
    # return HttpResponse(dump, content_type='application/json')
    status1= status.HTTP_200_OK
    naat_listt = JsonResponse(data = {'Naat_List':serializer.data,
                                      'Status':newdict},  status= status.HTTP_200_OK)

    return naat_listt


@api_view(['GET'])
@permission_classes((permissions.AllowAny,))
def artistlist(request):
    dataaa = Artist.objects.all()
    serializer = ArtistSerializer(dataaa, many=True)
    newdict = {'statuscode': 200, 'message': "Artist List"}
    naat_listt = JsonResponse(data={'Artist_List': serializer.data,
                                    'Status': newdict}, status=status.HTTP_200_OK)

    return naat_listt

@api_view(['GET'])
@permission_classes((permissions.AllowAny,))
def singlenaatgetpost(request):
    if request.method == 'POST':
        artistid = request.POST['artistid']
        try:
            instance = Naat.objects.filter(id=artistid)
        except Naat.DoesNotExist:
            instance = None
        if instance != None:
            serializer = NaatSerializer(instance)
            newdict = {'statuscode': 200, 'message': "Artist With Naat Found"}
            naat_listt = JsonResponse(data={'Naat_List_With_Artist': serializer.data,
                                            'Status': newdict}, status=status.HTTP_200_OK)

            return naat_listt
        else:
            return JsonResponse(status=status.HTTP_400_BAD_REQUEST)


@permission_classes((permissions.AllowAny,))
class Createartist(generics.ListCreateAPIView):
    """This class defines the create behavior of our rest api."""
    queryset = Artist.objects.all()
    serializer_class = ArtistSerializer

    def perform_create(self, serializer):
        """Save the post data when creating a new bucketlist."""
        serializer.save()

@permission_classes((permissions.AllowAny,))
class Createnaat(generics.ListCreateAPIView):
    """This class defines the create behavior of our rest api."""
    queryset = Naat.objects.all()
    serializer_class = NaatSerializer

    def perform_create(self, serializer):
        """Save the post data when creating a new bucketlist."""
        serializer.save()


@api_view(['POST'])
@permission_classes((permissions.AllowAny,))
def postingartist(request):
    if request.method == 'POST':
        serializer = ArtistSerializer(data=request.data)
        if serializer.is_valid():
            serializer.save()
            return JsonResponse(serializer.data, status=status.HTTP_201_CREATED)
        return JsonResponse(serializer.errors, status=status.HTTP_400_BAD_REQUEST)


