from django.db import models


class Artist(models.Model):
    name = models.CharField(max_length=32)
    country = models.CharField(max_length=32)
    style = models.CharField(max_length=32)
    class Meta:
        verbose_name_plural = 'Artist'
    def __str__(self):
        return '%s%s%s' % (self.name,self.country,self.style)

class Naat(models.Model):
    name = models.CharField(max_length=100)
    description = models.CharField(max_length=250)
    uploadedat = models.FileField(upload_to='naat')
    artist = models.ForeignKey(Artist, on_delete=models.CASCADE, null=True)
    class Meta:
        verbose_name_plural = 'Naat'
    def __str__(self):
        return '%s%s%s' % (self.name,self.description,self.uploadedat)

class Publisher(models.Model):
    name = models.CharField(max_length=70)
    naat = models.ManyToManyField(Naat)
    class Meta:
        verbose_name_plural = 'Publisher'
    def __str__(self):
        return '%s' % (self.name)

class Playlist(models.Model):
    name = models.CharField(max_length=50)
    naat = models.ManyToManyField(Naat)
    class Meta:
        verbose_name_plural = 'Playlist'
    def __str__(self):
        return '%s' % (self.name)