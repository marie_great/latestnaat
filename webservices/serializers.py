from rest_framework import serializers
from webservices.models import *


class NaatSerializer(serializers.ModelSerializer):
    class Meta:
        model = Naat
        fields = '__all__'

class ArtistSerializer(serializers.ModelSerializer):
    class Meta:
        model = Artist
        fields = '__all__'

