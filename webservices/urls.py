from django.conf.urls import url, include
from rest_framework import serializers, viewsets, routers
from webservices import views
from rest_framework.urlpatterns import format_suffix_patterns

urlpatterns = [
    url(r'^naatlist/$', views.naatlist, name='naatlist'),
    url(r'^artistlist/$', views.artistlist, name='artistlist'),
    url(r'^naat/get/(?P<artistid>[0-9]+)/$', views.naatofartistgeturl, name='naatgetpost'),
    url(r'^naat/get/$', views.singlenaatgetpost, name='naatgeturl'),
    url(r'^artist/get/(?P<artistid>[0-9]+)/$', views.singleartistget, name='artistget'),
    url(r'^createartist/$', views.Createartist.as_view(), name="createartist"),
    url(r'^createnaat/$', views.Createnaat.as_view(), name="createnaat"),
    url(r'^artistcreate/$', views.postingartist, name="artistcreate"),
]


urlpatterns = format_suffix_patterns(urlpatterns)